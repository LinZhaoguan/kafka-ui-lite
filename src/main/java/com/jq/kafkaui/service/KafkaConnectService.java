package com.jq.kafkaui.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jq.kafkaui.dao.KafkaConnectSourceDao;
import com.jq.kafkaui.domain.Auth;
import com.jq.kafkaui.domain.KafkaSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class KafkaConnectService {


    @Autowired
    KafkaConnectSourceDao kafkaConnectSourceDao;

    public List<KafkaSource> getAllSource() {
        return kafkaConnectSourceDao.getAll();
    }

    public List<KafkaSource> getAllSourceAuth() {
        List<KafkaSource> list = kafkaConnectSourceDao.getAll();
        list.forEach(t -> {
            Auth auth = kafkaConnectSourceDao.getAuthBySource(t.getId());
            JSONObject authO = new JSONObject();
            authO.put("add", auth.getAdd_auth() == 1);
            authO.put("update", auth.getUpdate_auth() == 1);
            authO.put("remove", auth.getRemove_auth() == 1);
            t.setAuth(authO);
        });
        return list;
    }

    public String getBroker(Integer sourceId) {
        return kafkaConnectSourceDao.selectById(sourceId);
    }

    @Transactional
    public void add(KafkaSource source) {
        kafkaConnectSourceDao.insert(source);
        kafkaConnectSourceDao.insertAuth(source.getId(), 0, 0, 0);
    }

    @Transactional
    public void deleteSource(Integer id) {
        kafkaConnectSourceDao.delete(id);
        kafkaConnectSourceDao.deleteAuth(id);
    }

    @Transactional
    public void auth(String param) {
        JSONObject jo = JSON.parseObject(param);
        Set<String> keys = jo.keySet();
        keys.forEach(key -> {
            JSONObject auth = jo.getJSONObject(key);
            int add = auth.getBoolean("add") ? 1 : 0;
            int update = auth.getBoolean("update") ? 1 : 0;
            int remove = auth.getBoolean("remove") ? 1 : 0;
            kafkaConnectSourceDao.updateAuth(Integer.parseInt(key), add, update, remove);
        });

    }
}
