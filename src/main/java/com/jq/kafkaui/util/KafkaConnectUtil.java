package com.jq.kafkaui.util;

import com.alibaba.fastjson.JSONObject;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @description: Kafka-Connect 工具类
 * @author: LinZhaoguan
 * @create: 2022-08-01 10:05
 **/
@Slf4j
@UtilityClass
public class KafkaConnectUtil {

    private static RestTemplate REST_TEMPLATE;

    static {
        try {
            SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
            requestFactory.setConnectTimeout(1000);
            requestFactory.setReadTimeout(6000);
            REST_TEMPLATE = new RestTemplate(requestFactory);
        } catch (Exception e) {
            log.error("err: {}", e.getMessage(), e);
        }
    }
    private static final String ENDPOINT_CONNECTORS = "/connectors";

    public List<JSONObject> getConnectors(String serverUrl) {
        ResponseEntity<List<String>> response = REST_TEMPLATE.exchange(serverUrl + ENDPOINT_CONNECTORS, HttpMethod.GET, null, new ParameterizedTypeReference<List<String>>() {
        });
        if (response.getStatusCode() != HttpStatus.OK) {
            log.error("远程调用异常: {} {}", response.getStatusCodeValue(), response.getBody());
            return Collections.emptyList();
        }
        return Objects.requireNonNull(response.getBody()).stream().map(connectorName -> {
            // 获取指定connector的信息
            String connectorInfoUrl = serverUrl + ENDPOINT_CONNECTORS + '/' + connectorName;
            JSONObject infoJson = REST_TEMPLATE.getForObject(connectorInfoUrl, JSONObject.class);
            if (infoJson == null) {
                infoJson = new JSONObject();
            }
            // 获取指定connector的状态
            String connectorStatusUrl = serverUrl + ENDPOINT_CONNECTORS + '/' + connectorName + "/status";
            JSONObject statusJson = REST_TEMPLATE.getForObject(connectorStatusUrl, JSONObject.class);
            infoJson.put("status", statusJson);
            return infoJson;
        }).collect(Collectors.toList());
    }


    public static JSONObject createConnector(String serverUrl, JSONObject config) {
        String connectorUrl = serverUrl + ENDPOINT_CONNECTORS;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<JSONObject> httpEntity = new HttpEntity<>(config, headers);

        return REST_TEMPLATE.postForObject(connectorUrl, httpEntity, JSONObject.class);
    }

    public static void deleteConnector(String serverUrl, String connectorName) {
        String connectorUrl = serverUrl + ENDPOINT_CONNECTORS + '/' + connectorName;
        REST_TEMPLATE.delete(connectorUrl);
    }

    public static JSONObject updateConnector(String serverUrl, String connectorName, JSONObject config) {
        String connectorUrl = serverUrl + ENDPOINT_CONNECTORS + '/' + connectorName + "/config";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<JSONObject> httpEntity = new HttpEntity<>(config, headers);
        ResponseEntity<JSONObject> response = REST_TEMPLATE.exchange(connectorUrl, HttpMethod.PUT, httpEntity, JSONObject.class);
        return response.getBody();
    }

    public static JSONObject changeState(String serverUrl, String connectorName, String option) {
        String connectorUrl = serverUrl + ENDPOINT_CONNECTORS + '/' + connectorName + '/' + option;
        HttpMethod httpMethod = "restart".equalsIgnoreCase(option) ? HttpMethod.POST : HttpMethod.PUT;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<JSONObject> httpEntity = new HttpEntity<>(headers);
        ResponseEntity<JSONObject> response = REST_TEMPLATE.exchange(connectorUrl, httpMethod, httpEntity, JSONObject.class);
        return response.getBody();
    }
}
