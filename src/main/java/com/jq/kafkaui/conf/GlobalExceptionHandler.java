package com.jq.kafkaui.conf;

import com.jq.kafkaui.dto.ResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;

/**
 * @author: LinZhaoguan
 * @date: 2022/8/2
 * @description: 全局异常拦截器
 * @modify:
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseDto handleHttpClientErrorException(HttpClientErrorException e) {
        log.error("远程调用异常: {}", e.getMessage(), e);
        if (e.getStatusCode() == HttpStatus.CONFLICT) {
            return ResponseDto.fail("Kafka-Connect: rebalance is in process");
        }
        return ResponseDto.fail("Kafka-Connect返回异常信息: " + e.getResponseBodyAsString());
    }

    @ExceptionHandler(Exception.class)
    public ResponseDto exception(Exception e) {
        log.error("全局拦截异常: {}", e.getMessage(), e);
        return ResponseDto.fail(e.getMessage());
    }

}
