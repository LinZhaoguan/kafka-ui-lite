package com.jq.kafkaui.controller;

import com.alibaba.fastjson.JSONObject;
import com.jq.kafkaui.domain.KafkaSource;
import com.jq.kafkaui.dto.ResponseDto;
import com.jq.kafkaui.service.KafkaConnectService;
import com.jq.kafkaui.util.KafkaConnectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: kafkaUI
 * @description:
 * @author: LinZhaoguan
 * @create: 2022-08-01 10:06
 **/
@RestController
@RequestMapping("/kafka/connect")
public class KafkaConnectController {

    @Autowired
    KafkaConnectService kafkaService;

    @RequestMapping("/add")
    public String addSource(KafkaSource source) {
        kafkaService.add(source);
        return "success";
    }

    @RequestMapping("/deleteSource/{id}")
    public String deleteSource(@PathVariable Integer id) {
        kafkaService.deleteSource(id);
        return "success";
    }

    @RequestMapping("/auth")
    public void auth(String param) throws Exception {
        kafkaService.auth(param);
    }

    @RequestMapping("/getSource")
    public List<KafkaSource> getAllSource() {
        return kafkaService.getAllSource();
    }

    @RequestMapping("/getAllSourceAuth")
    public List<KafkaSource> getSourceAuth() {
        return kafkaService.getAllSourceAuth();
    }

    @RequestMapping("/getConnectors")
    public ResponseDto getConnectors(Integer sourceId) {
        String brokers = kafkaService.getBroker(sourceId);
        return ResponseDto.success(KafkaConnectUtil.getConnectors(brokers));
    }

    @PostMapping("/createConnector")
    public ResponseDto createConnector(@RequestParam("sourceId") Integer sourceId, @RequestBody JSONObject config) {
        String brokers = kafkaService.getBroker(sourceId);
        return ResponseDto.success(KafkaConnectUtil.createConnector(brokers, config));
    }

    @PostMapping("/updateConnector")
    public ResponseDto updateConnector(@RequestParam("sourceId") Integer sourceId,
                                       @RequestBody JSONObject config) {
        String brokers = kafkaService.getBroker(sourceId);
        return ResponseDto.success(KafkaConnectUtil.updateConnector(brokers, config.getString("name"), config.getJSONObject("config")));
    }

    @PostMapping("/deleteConnector")
    public ResponseDto deleteConnector(@RequestParam("sourceId") Integer sourceId, @RequestParam("connectorName") String connectorName) {
        String brokers = kafkaService.getBroker(sourceId);
        KafkaConnectUtil.deleteConnector(brokers, connectorName);
        return ResponseDto.success();
    }

    @PostMapping("/changeState")
    public ResponseDto changeState(@RequestParam("sourceId") Integer sourceId,
                                   @RequestParam("connectorName") String connectorName,
                                   @RequestParam("option") String option) {
        String brokers = kafkaService.getBroker(sourceId);
        return ResponseDto.success(KafkaConnectUtil.changeState(brokers, connectorName, option));
    }


}
