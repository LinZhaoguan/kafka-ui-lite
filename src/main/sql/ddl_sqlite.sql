CREATE TABLE "kafka_auth"
(
    "id"          integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "source_id"   INTEGER NOT NULL,
    "add_auth"    integer,
    "update_auth" integer,
    "remove_auth" integer
);

CREATE TABLE "zookeeper_auth"
(
    "id"          integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "source_id"   INTEGER NOT NULL,
    "add_auth"    integer,
    "update_auth" integer,
    "remove_auth" integer
);

CREATE TABLE "redis_auth"
(
    "id"          integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "source_id"   INTEGER NOT NULL,
    "add_auth"    integer,
    "update_auth" integer,
    "remove_auth" integer
);

CREATE TABLE "kafka_source"
(
    "id"     INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name"   text(64),
    "broker" text(64)
);

CREATE TABLE "zookeeper_source"
(
    "id"      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "address" TEXT,
    "name"    TEXT
);

CREATE TABLE "redis_source"
(
    "id"       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name"     TEXT,
    "ip"       TEXT,
    "port"     integer,
    "password" TEXT
);

create table kafka_connect_source
(
    id     INTEGER not null
        primary key autoincrement,
    name   text(64) not null,
    broker text(64) not null
);

create table kafka_connect_auth
(
    id          INTEGER not null
        primary key autoincrement,
    source_id   INTEGER not null,
    add_auth    INTEGER,
    update_auth INTEGER,
    remove_auth INTEGER
);
