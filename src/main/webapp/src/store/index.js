import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    kafkaAuth: {},
    kafkaConnectAuth: {},
    zkAuth: {},
    redisAuth: {},
    kafkaId: null
  },
  mutations: {
    setKafkaAuth(state, payload) {
      state.kafkaAuth = payload
    },
    setKafkaConnectAuth(state, payload) {
      state.kafkaConnectAuth = payload
    },
    setZKAuth(state, payload) {
      state.zkAuth = payload
    },
    setRedisAuth(state, payload) {
      state.redisAuth = payload
    },
    setKafkaId(state, kafkaId) {
      state.kafkaId = kafkaId
    }
  },
  actions: {},
  getters: {
    getKafkaAuth: (state) => (sourceId) => {
      let a = state.kafkaAuth[sourceId]
      console.log("getters...", a)
      return a
    },
    getKafkaConnectAuth: (state) => (sourceId) => {
      let a = state.kafkaConnectAuth[sourceId]
      console.log("getters...", a)
      return a
    },
    getZKAuth: (state) => (sourceId) => {
      let a = state.zkAuth[sourceId]
      return a
    },
    getRedisAuth: (state) => (sourceId) => {
      return state.redisAuth[sourceId]
    }
  }

})
